<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = md5($data['password']).md5($data['password'].'2');
        $client->setPassword($password);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);

        return $client;
    }
}
